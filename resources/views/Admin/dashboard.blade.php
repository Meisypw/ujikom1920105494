@extends('layouts.pembimbing.dashboard')

@section('body')

  <div class="row">

    <div class="col-lg-5 d-flex justify-content-center">
      <img src="/img/2.png" alt="" id="image" style="width: 470px">
    </div>

    <div class="col-lg-7 mt-4">
      
      <div class="ml-5 mt-3">
        <span class="text-uppercase" id="title" style="font-weight: 500">menu</span>
      </div>
      
      <div class="row" id="menu" style="margin-left: 33px; margin-top: 50px">
        <div class="col-lg-3" id="kegiatan">
          <a href="/dashboard/jurnalSiswa" class="btn text-dark" style="background-color: #F08080; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/jurusan.svg" alt="" style="margin-top: 7px; margin-left: 1px"></a>
        </div>
        <div class="col-lg-3" id="pengajuan">
          <a href="/dashboard/Pengajuan_PKL" class="btn text-dark" style="background-color: #F08080	; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/pengajuanPKL.svg" alt="" style="margin-top: 6px; margin-right: 1px"></a>
        </div>
        <div class="col-lg-3" id="monitoring">
          <a href="/dashboard/monitoring" class="btn text-dark" style="background-color: #F08080	; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/profile.svg" alt="" style="margin-top: 7.8px"></a>
        </div>
        <div class="col-lg-3" id="tabel">
          <a href="/table" class="btn text-dark" style="background-color: #F08080	; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/table.svg" alt="" style="margin-top: 7.8px"></a>
        </div>
      </div>

      <div class="row" id="label">
        <div class="col-lg-3 text-center" style="margin-left: 7px">
          <span id="cekKegiatan">Cek Kegiatan<br>Siswa</span>
        </div>
        <div class="col-lg-3 text-center" style="margin-left: -12px">
          <span id="pengajuanPKL">Pengajuan<br>PKL</span>
        </div>
        <div class="col-lg-3 text-center" style="margin-left: -11px">
          <span id="monitoringLabel">Monitoring</span>
        </div>
        <div class="col-lg-3 text-center" style="margin-left: -10px">
          <span id="tabelLabel">Tabel</span>
        </div>
      </div>
      <a href="/setting">setting</a>
      <button type="button" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Buat Akun</button>

      <div class="row" id="kembali">
        <div class="col-lg-12 d-flex justify-content-end mr-2 mb-2" style="margin-top: 112px; margin-left: -66px">
          <a href="#" class="btn btn-md" style="background-color: #F08080	; color: black">Kembali</a>
        </div>
      </div>

    </div>

  </div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="/createAccount" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                <div class="card-body">
                <h3>Profile</h3>
                    <div class="mb-3">
                    <label for="" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="name" id="" required >
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">NIS/NIP</label>
                    <input type="text" class="form-control" name="nis" id="" required >
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Kelas</label>
                    <select class="form-control" name="kelas" id="" style="height: 40px">
                        <option selected value="">--Pilih Kelas--</option>
                        <option value="X">X</option>
                        <option value="XI">XI</option>
                        <option value="XII">XII</option>
                    </select>
                    <small id="helpId" class="form-text text-muted">opsional</small>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Jurusan</label>
                    <select class="form-control" name="jurusan" id="" style="height: 40px" required>
                      <option selected value="">--Pilih Jurusan--</option>
                      @foreach ($jurusan as $jurusan)
                        <option value="{{ $jurusan->id }}">{{ $jurusan->jurusan }}</option>
                      @endforeach
                    </select>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Foto</label>
                    <input type="file" class="form-control" name="foto" id="" required>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Level</label>
                      <select class="form-control" name="level" id="" style="height: 40px" required>
                          <option selected>--Pilih Level--</option>
                        <option value="pembimbing">Pembimbing</option>
                        <option value="siswa">Siswa</option>
                      </select>
                    </div>
                </div>
                </div>
                <div class="card">
                <div class="card-body">
                <h3>User Information</h3>
                    <div class="mb-3">
                      <label for="" class="form-label">Username</label>
                      <input type="text" class="form-control" name="username" required >
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">email</label>
                      <input type="email" class="form-control" name="email" required>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">password</label>
                      <input type="password" class="form-control" name="password" required>
                    </div>
                </div>
                </div>
        
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Buat Akun</button>
              </form>
            </div>
          </div>
        </div>
      </div>

@endsection