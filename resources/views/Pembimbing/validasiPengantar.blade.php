@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card" >
             <h5 class="card-header">Validasi Surat Pengantar</h5>
            <div class="card-body">
                
                @foreach ($data as $item)
                    <h4>Nis: {{ $item->nis }}</h4>
                    <h4 >Nama: {{ $item->name }}</h4>
                    <form action="/download" method="post">
                        @csrf
                            <input type="hidden" name="pengantar_pkl" value="{{ $item->pengantar_pkl }}">
                            <input type="submit" class="btn btn-primary" value="Download Pengantar">
                    </form>
                @endforeach
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" style="margin-top: 12px">Validasi</button>

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/prosesValidasiPengantar" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Keterangan:</label>
                            <div class="input-group">
                            @foreach ($data as $item)
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            @endforeach
                            <select name="status">
                                <option selected>Choose...</option>
                                <option value="Memilih Tempat Prakerin">Terima Lembar pengesahan</option>
                                <option value="Lembar pengesahan di tolak">Tolak Lembar Pengesahan</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Message:</label>
                            <textarea class="form-control" id="message-text" name="ket_message_pengantar"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>
            </div>        
        </div>
    </div>

@endsection