<?php

namespace App\Http\Controllers;

use App\Models\pembimbing;
use App\Models\perusahaan;
use App\Models\jurnal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\laporan;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;


class PembimbingController extends Controller
{
    public function dashboard()
    {
        return view('Pembimbing.dashboard');
    }
    public function monitoring()
    {
        $data = ['data' => User::all()->where('level', "siswa")->sortBy('name')];
        // $data1 = ['pembimbing' => pembimbing::all()];
        return view('Pembimbing.home', $data,);
    }
    public function jurnalSiswa()
    {
        $data = ['data' => User::all()->where('level', "siswa")];
        return view('Pembimbing.jurnalSiswa', $data);
    }
    public function cekKegiatan($id)
    {
        $data = ['data' => jurnal::all()->where('User_id', $id)];
        return view('Pembimbing.cekKegiatan', $data);
    }
    public function Pengajuan_PKL()
    {
        $data = ['data' => User::all()->sortBy('name')];
        $data1 = ['data1' => Pembimbing::all()->sortBy('name')];
        return view('Pembimbing.Pengajuan_PKL', $data, $data1);
    }
    public function updateKeteranganJurnal(Request $request)
    {
        DB::table('jurnals')->where('id', $request->id)->update([
            'keterangan' => $request->keterangan,
        ]);

        return redirect()->back();
    }
    public function showAllPembimbing()
    {
        $data = ['data' => pembimbing::all()];
        return view('Pembimbing.pembimbingTable', $data);
    }
    public function createPembimbing()
    {
        return view('Pembimbing.formCreate');
    }
    public function insertPembimbing(Request $request)
    {
        DB::table('users')->insert([
            'name' => $request->name,
            'nis' => $request->nip,
            'level' => $request->level,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password)

        ]);
        DB::table('pembimbings')->insert([
            'name' => $request->name,
            'nip' => $request->nip,
            'kejuruan' => $request->kejuruan
        ]);
        return redirect()->back();
    }
    public function validasiPengantar($id)
    {
        $data = ['data' => user::all()->where('id', $id)];
        return view('Pembimbing.validasiPengantar', $data);
    }
    public function download(Request $request)
    {
        $file_path = public_path('data_file/' . $request->pengantar_pkl);
        return response()->download($file_path);
    }
    public function prosesValidasiPengantar(Request $request)
    {
        DB::table('users')->where('id', $request->id)->update([
            'ket_message_pengantar' => $request->ket_message_pengantar,
            'status' => $request->status
        ]);
        return redirect()->back();
    }
    public function konfirmasiPengajuan($id)
    {
        $pembimbing = ['pembimbing' => pembimbing::all()];
        $data = ['data' => user::all()->where('id', $id)];
        return view('Pembimbing.konfirmasiPengajuan', $data, $pembimbing);
    }
    public function addPembimbing(Request $request)
    {
        DB::table('users')->where('id', $request->id)->update([
            'status' => $request->status,
            'pembimbing_id' => $request->pembimbing_id
        ]);
        return redirect('/home');
    }
    public function proseskonfirmasiPengajuan(Request $request)
    {
        DB::table('users')->where('id', $request->id)->update([
            'status' => $request->status,
            'mulai_pkl' => $request->mulai_pkl,
            'selesai_pkl' => $request->selesai_pkl
        ]);
        return redirect()->back();
    }
    public function detailPembimbing($id)
    {
        $pembimbing = ['pembimbing' => pembimbing::all()->where('id', $id)];
        $data = ['data' => user::all()->where('pembimbing_id', $id)];
        return view('Pembimbing.detail', $data, $pembimbing);
    }
    public function cekLaporan()
    {
        $data = ['data' => User::all()->where('laporan_id', !null)];
        return view('Pembimbing.cekLaporan', $data);
    }
    public function validasiLaporan(Request $request)
    {
        DB::table('laporans')->where('id', $request->id)->update([
            'keterangan' => $request->keterangan,
            'coment' => $request->coment,
        ]);
        return redirect()->back();
    }
}
